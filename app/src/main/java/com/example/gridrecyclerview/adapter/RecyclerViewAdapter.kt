package com.example.gridrecyclerview.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.gridrecyclerview.R
import com.example.gridrecyclerview.models.UserModel
import kotlinx.android.synthetic.main.female_user_recyclerview_layout.view.*
import kotlinx.android.synthetic.main.male_user_recyclerview_layout.view.*


class RecyclerViewAdapter(
    private val users: MutableList<UserModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val MALE_USER = 1
        const val FEMALE_USER = 2
    }


    inner class MaleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind() {
            val model = users[adapterPosition]
            itemView.usernameTextView.text = model.username
            itemView.maleImageView.setImageResource(R.mipmap.male)

        }
    }

    inner class FemaleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val model = users[adapterPosition]
            itemView.username2TextView.text = model.username
            itemView.femaleImageView.setImageResource(R.mipmap.female)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == MALE_USER){
            MaleViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.male_user_recyclerview_layout,
                    parent,
                    false))
        } else{
            FemaleViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.female_user_recyclerview_layout,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is MaleViewHolder)
            holder.onBind()
        else if(holder is FemaleViewHolder)
            holder.onBind()

    }

    override fun getItemViewType(position: Int): Int {
        val model = users[position]
        return if(model.gender == "Male") {
             MALE_USER
        } else{
            FEMALE_USER
        }

    }



}
package com.example.gridrecyclerview.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.gridrecyclerview.R
import com.example.gridrecyclerview.adapter.RecyclerViewAdapter
import com.example.gridrecyclerview.models.UserModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val userList = mutableListOf<UserModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        adapter = RecyclerViewAdapter(userList)
        recyclerView.layoutManager = GridLayoutManager(this, 2)

        recyclerView.adapter = adapter
        setData()

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                setData()
                adapter.notifyDataSetChanged()
            }, 2000)
        }

    }

    private fun refresh() {
        userList.clear()
        adapter.notifyDataSetChanged()

    }

    private fun setData() {
        userList.add(
            0,
            UserModel("Ana Rusadze", "Female")
        )
        userList.add(
            0,
            UserModel("Nika Tabatadze", "Male")
        )
        userList.add(
            0,
            UserModel("Sandro kakhetelidze", "Male")
        )
        userList.add(
            0,
            UserModel("Lika Ghlonti", "Female")
        )
        userList.add(
            0,
            UserModel("Ana Morchiladze", "Female")
        )
        userList.add(
            0,
            UserModel("Giga Sulkhanashvili", "Male")
        )
        userList.add(
            0,
            UserModel("Giorgi Natsvlishvili", "Male")
        )
        userList.add(
            0,
            UserModel("Davit Soitashvili", "Male")
        )
        userList.add(
            0,
            UserModel("Davit Beriashvili", "Male")
        )
        userList.add(
            0,
            UserModel("Lika Diasamidze", "Female")
        )


    }


}
